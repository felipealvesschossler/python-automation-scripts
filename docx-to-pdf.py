from docx2pdf import convert
import os

file_folder = input('Type the path/file that you want convert to PDF(absolute path): \n')
file_folder_destiny = input('Type the destiny folder: \n')

add_backslash = file_folder.replace('\\', '\\\\')
file_folder = add_backslash

add_backslash = file_folder_destiny.replace('\\', '\\\\')
file_folder_destiny = add_backslash

convert(file_folder, file_folder_destiny)
