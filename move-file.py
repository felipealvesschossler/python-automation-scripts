import os
import shutil

file = ''
origin_path = ''
origin_path_file = ''
destiny_path = ''
destiny_path_file = ''

file = input('What file do you want to move? \n')

origin_path = input('Move this file from path? \n')
origin_path_file = (origin_path + '/' + file)

destiny_path = input('To path? \n')
destiny_path_file = (destiny_path + '/' + file)

shutil.move(origin_path_file, destiny_path_file)
print('The file was moved to ' + destiny_path)