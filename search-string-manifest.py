"""
Script criado para procurar em todos os deploys a string desejada.
"""
import os

def output_all_yamls():
    input_context = input("Digite o contexto desejado: ")
    input_string = input("Digite a string desejada: ")

    os.system('kubectl get deploy --context='+input_context+' -o yaml \
                        > yaml_outputs.txt')

    with open('yaml_outputs.txt', 'r') as read_obj:
        for line in read_obj:
            if input_string in line:
                print(line)
    os.remove("yaml_outputs.txt")

output_all_yamls()